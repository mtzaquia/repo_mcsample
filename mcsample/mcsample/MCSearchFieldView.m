//
//  MCSearchFieldView.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCSearchFieldView.h"

@interface MCSearchFieldView () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *fieldKeywords;
@property (weak, nonatomic) IBOutlet UIButton *buttonSearch;


@end

@implementation MCSearchFieldView {
    UIVisualEffectView *_blurView;
}

static MCSearchFieldView *instance = nil;

+(void)showWithDelegate:(id<MCSearchFieldViewDelegate>)delegate {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ // Ah, thread-safety is beautiful.
        instance = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil][0];
        instance.layer.shadowOffset = CGSizeMake(1, 1);
        instance.layer.shadowColor = [[UIColor blackColor] CGColor];
        instance.layer.shadowRadius = 3.0;
        instance.layer.shadowOpacity = 0.8;
        instance.layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:instance.layer.bounds cornerRadius: 10] CGPath];
    });

    [instance fadeInWithDelegate:delegate];

}

+(void)hide {
    [instance fadeOut];
}

-(void)fadeInWithDelegate:(id<MCSearchFieldViewDelegate>)delegate {

    self.delegate = delegate;
    self.fieldKeywords.delegate = self;
    self.transform = CGAffineTransformMakeScale(0.01, 0.01);

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fadeOut)];

    _blurView = [[UIVisualEffectView alloc] initWithEffect: [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    _blurView.alpha = 0;
    [_blurView addGestureRecognizer:tapRecognizer];

    self.fieldKeywords.text = nil;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkButton) name:UITextFieldTextDidChangeNotification object:self.fieldKeywords];
    [self checkButton];

    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:_blurView];
        [window addSubview:self];
        _blurView.frame = window.bounds;
        self.center = CGPointMake(window.frame.size.width/2, window.frame.size.height/2 - 50);

        //TODO: Add constraints in case of a screen rotation.

        [UIView animateWithDuration:0.25 animations:^{
            _blurView.alpha = 1;
            self.alpha = 1;
            self.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {
            [self.fieldKeywords becomeFirstResponder];
        }];
    });


}

-(void)fadeOut {
    [UIView animateWithDuration:0.25 animations:^{
        _blurView.alpha = 0;
        self.alpha = 0;
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [_blurView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

- (IBAction)search:(UIButton *)sender {
    [self textFieldShouldReturn:self.fieldKeywords];
}

- (void)checkButton {

    [UIView animateWithDuration:0.25 animations:^{
        BOOL invalidInput = ([self.fieldKeywords.text isEqualToString:@""] || self.fieldKeywords.text == nil);
        self.buttonSearch.userInteractionEnabled = !invalidInput;
        self.buttonSearch.alpha = invalidInput ? 0.5 : 1;
    }];

}

//MARK: UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {

    if (![textField.text isEqualToString:@""] && textField.text != nil) {

        [self.delegate searchFieldView:self didRequestSearchWithKeywords:textField.text];

    }

    [MCSearchFieldView hide];
    return NO;

}

@end
