//
//  UIImageView+AsyncLoad.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCAsyncLoadImageView.h"

#define PLACEHOLDER_IMAGE_NAME @"AlbumPlaceholder"

@implementation MCAsyncLoadImageView {
    NSMutableURLRequest *_request;
    NSURLSessionDataTask *_task;
}

-(void)startLoadingImageFromURL:(NSURL *)URL {

    _request = [NSMutableURLRequest requestWithURL:URL];

    NSURLSession *session = [NSURLSession sharedSession];
    _task = [session dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        if (error != nil) { // In case of an error, we just ignore and rollback to the placeholder.
            [self revertToPlaceholder];
            return;
        }

        //INFO: not needed for such small images, but who knows.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *imageFromData = [UIImage imageWithData:data];
            if (imageFromData != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = imageFromData;
                });
            }
        });

    }];

    [_task resume];

}

-(void)stopLoading {

    [_task cancel];
    _task = nil;
    _request = nil;
    [self revertToPlaceholder];

}

-(void)revertToPlaceholder {

    dispatch_async(dispatch_get_main_queue(), ^{
        self.image = [UIImage imageNamed:PLACEHOLDER_IMAGE_NAME];
    });

}

@end
