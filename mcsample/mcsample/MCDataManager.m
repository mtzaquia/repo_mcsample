//
//  DataManager.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCDataManager.h"
#import "MCResult.h"
#import "NSString+URLEncoding.h"

#define ENDPOINT_BASE   @"http://itunes.apple.com/search?term="
#define KEY_RESULTS     @"results"

@implementation MCDataManager

+(void)performSearchWithString:(NSString *)string andCallback:(void (^)(NSArray*, NSError*))callback {

    NSString *queryString = [string URLEncoded];

    NSURL *URL = [NSURL URLWithString:[ENDPOINT_BASE stringByAppendingString:queryString]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = 15.0; // setting a short timeout.

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {

        //INFO: Errors are thrown and handled on the higher level (controller), since we'll only show an alert (UI).

        if (error != nil) { // something went wrong with the data task.
            callback(nil, error);
            return;
        }

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

            NSError *conversionError;
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&conversionError];

            if (conversionError != nil) { // something went wrong with the JSON conversion.
                callback(nil, conversionError);
                return;
            }

            NSMutableArray *convertedResults = [@[] mutableCopy];
            for (NSDictionary *rawResultEntry in object[KEY_RESULTS]) {
                [convertedResults addObject:[[MCResult alloc] initWithDictionary:rawResultEntry]];
            }
            

            callback([convertedResults copy], nil);

        });

    }];

    [task resume];

}

@end
