//
//  MCLoaderView.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCLoaderView : UIVisualEffectView

+(void)show;
+(void)hide;

@end
