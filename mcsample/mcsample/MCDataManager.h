//
//  DataManager.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCDataManager : NSObject

+(void)performSearchWithString:(NSString *)string andCallback:(void (^)(NSArray *, NSError *))callback;

@end
