//
//  UIImageView+AsyncLoad.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCAsyncLoadImageView : UIImageView

-(void)startLoadingImageFromURL:(NSURL *)URL;
-(void)stopLoading;
-(void)revertToPlaceholder;

@end
