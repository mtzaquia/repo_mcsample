//
//  DetailViewController.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/25/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "DetailViewController.h"
#import "MCUtils.h"
#import "MCAsyncLoadImageView.h"
#import "MCResult.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet MCAsyncLoadImageView *imageAlbumCover;
@property (weak, nonatomic) IBOutlet UILabel *labelTrackName;
@property (weak, nonatomic) IBOutlet UILabel *labelArtistName;
@property (weak, nonatomic) IBOutlet UILabel *labelAlbumName;
@property (weak, nonatomic) IBOutlet UILabel *labelReleaseDate;
@property (weak, nonatomic) IBOutlet UIView *viewPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.imageAlbumCover.layer.cornerRadius = self.imageAlbumCover.frame.size.height/2;
    self.viewPrice.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:127.0/255.0 blue:0.0/255.0 alpha:1.0].CGColor;

    [self.imageAlbumCover startLoadingImageFromURL:self.result.albumCoverURL];
    self.labelTrackName.text = self.result.trackName;
    self.labelArtistName.text = self.result.artistName;
    self.labelAlbumName.text = self.result.albumName;
    self.labelReleaseDate.text = [MCUtils yearFromDate:self.result.releaseDate];
    self.labelPrice.text = [NSString stringWithFormat:@"%@ %.2f", self.result.currency, self.result.songPrice.floatValue];

}

@end
