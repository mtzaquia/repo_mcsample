//
//  MCSearchFieldView.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCSearchFieldViewDelegate;
@interface MCSearchFieldView : UIView

@property (nonatomic, weak) id<MCSearchFieldViewDelegate> delegate;

//INFO: Using static methods for convenience, but we're dealing with a singleton.
+(void)showWithDelegate:(id<MCSearchFieldViewDelegate>)delegate;
+(void)hide;

@end

@protocol MCSearchFieldViewDelegate

@required
-(void)searchFieldView:(MCSearchFieldView *)searchFieldView didRequestSearchWithKeywords:(NSString *)keywords;

@end