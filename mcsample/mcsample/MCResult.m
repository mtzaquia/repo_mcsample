//
//  MCResult.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCResult.h"
#import "MCUtils.h"

//INFO: #define within .m files is fine, as they're considered private. So no scope problems here.
#define KEY_ARTIST_ID       @"artistId"
#define KEY_ARTIST_NAME     @"artistName"
#define KEY_ALBUM_COVER     @"artworkUrl100"
#define KEY_ALBUM_NAME      @"collectionName"
#define KEY_ALBUM_PRICE     @"collectionPrice"
#define KEY_SONG_NAME       @"trackName"
#define KEY_SONG_PRICE      @"trackPrice"
#define KEY_PRICE_CURRENCY  @"currency"
#define KEY_RELEASE_DATE    @"releaseDate"

@implementation MCResult

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];
    if (self) {

        self.artistID = dictionary[KEY_ARTIST_ID];
        self.albumCoverURL = [NSURL URLWithString:dictionary[KEY_ALBUM_COVER]];
        self.albumName = dictionary[KEY_ALBUM_NAME];
        self.artistName = dictionary[KEY_ARTIST_NAME];
        self.trackName = dictionary[KEY_SONG_NAME];
        self.albumPrice = [NSNumber numberWithFloat:[dictionary[KEY_ALBUM_PRICE] floatValue]];
        self.songPrice = [NSNumber numberWithFloat:[dictionary[KEY_SONG_PRICE] floatValue]];
        self.releaseDate = [[MCUtils sharedInstance].sharedFormatter dateFromString:dictionary[KEY_RELEASE_DATE]];
        self.currency = dictionary[KEY_PRICE_CURRENCY];

    }

    return self;

}

@end