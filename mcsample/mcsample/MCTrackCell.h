//
//  MCTrackCell.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MCAsyncLoadImageView;
@interface MCTrackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MCAsyncLoadImageView   *imageAlbumCover;
@property (weak, nonatomic) IBOutlet UILabel                *labelTrackName;
@property (weak, nonatomic) IBOutlet UILabel                *labelArtistName;

@end
