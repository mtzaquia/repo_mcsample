//
//  ViewController.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MasterViewController.h"
#import "MCDataManager.h"
#import "MCResult.h"
#import "MCTrackCell.h"
#import "MCAsyncLoadImageView.h"
#import "MCLoaderView.h"
#import "MCSearchFieldView.h"
#import "DetailViewController.h"

#define IDENTIFIER_SEGUE_MASTER_TO_DETAIL @"master_to_detail"
#define IDENTIFIER_TRACK_CELL @"TrackCell"
#define IDENTIFIER_DETAIL_VIEW_CONTROLLER @"DetailViewController"

@interface MasterViewController () <UITableViewDataSource, UITableViewDelegate, MCSearchFieldViewDelegate, UIViewControllerPreviewingDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewNoResults;

@end

@implementation MasterViewController {
    NSArray *_results;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib

    [self registerForPreviewingWithDelegate:self sourceView:self.tableView];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:IDENTIFIER_SEGUE_MASTER_TO_DETAIL]) {
        DetailViewController *vc = segue.destinationViewController;
        vc.result = _results[self.tableView.indexPathForSelectedRow.row];
    }
}

//MARK: UIViewControllerPreviewingDelegate
- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {

    NSIndexPath *idxPath = [self.tableView indexPathForRowAtPoint:location];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idxPath];

    previewingContext.sourceRect = cell.frame;

    DetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_DETAIL_VIEW_CONTROLLER];
    vc.result = _results[idxPath.row];

    vc.preferredContentSize = CGRectInset(vc.view.frame, 0, 200).size;

    return vc;

}

//MARK: Custom actions
- (IBAction)showSearchField:(id)sender {
    [MCSearchFieldView showWithDelegate:self];
}

//MARK: UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [UIView animateWithDuration:0.25 animations:^{
        tableView.alpha = (_results == nil ? 1 : (_results.count > 0 ? 1 : 0)); // This gives us a nice effect on data refresh.
        self.viewNoResults.alpha = _results != nil && _results.count == 0 ? 1 : 0;
    }];



    return _results.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    MCTrackCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_TRACK_CELL forIndexPath:indexPath];

    MCResult *currentEntry = _results[indexPath.row];
    [cell.imageAlbumCover startLoadingImageFromURL:currentEntry.albumCoverURL];
    cell.labelTrackName.text = currentEntry.trackName;
    cell.labelArtistName.text = [currentEntry.artistName uppercaseString]; // fancy appearance, hehe

    return cell;

}

//MARK: UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//MARK: MCSearchFieldViewDelegate
-(void)searchFieldView:(MCSearchFieldView *)searchFieldView didRequestSearchWithKeywords:(NSString *)keywords {

    [MCLoaderView show];
    [MCDataManager performSearchWithString:keywords andCallback:^(NSArray *results, NSError *error) {

        [MCLoaderView hide];

        if (error != nil) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ERROR_TITLE", nil) message:NSLocalizedString(@"ERROR_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"DISMISS", nil) style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }

        _results = results;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        });
        
    }];


}

@end
