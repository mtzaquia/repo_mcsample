//
//  MCUtils.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCUtils.h"

#define DATE_FORMAT @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'" // Format from server.

@implementation MCUtils

+(instancetype)sharedInstance {

    static MCUtils *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [MCUtils new];
        instance.sharedFormatter = [NSDateFormatter new];
        [instance.sharedFormatter setDateFormat:DATE_FORMAT];
    });

    return instance;

}

+(void)applyAppearance {
    [[UINavigationBar appearance] setBarTintColor:[UIColor orangeColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
}

+(NSString *)yearFromDate:(NSDate *)date {

    NSInteger year = [[NSCalendar currentCalendar] component:NSCalendarUnitYear fromDate:date];
    return [NSString stringWithFormat:@"%.0ld", (long)year];

}

@end
