//
//  DetailViewController.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/25/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MCResult;
@interface DetailViewController : UIViewController

@property (nonatomic, retain) MCResult *result;

@end
