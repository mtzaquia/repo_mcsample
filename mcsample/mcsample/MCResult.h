//
//  MCResult.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCResult : NSObject

@property (nonatomic, strong) NSString  *artistID;
@property (nonatomic, strong) NSURL     *albumCoverURL;
@property (nonatomic, strong) NSString  *albumName;
@property (nonatomic, strong) NSString  *artistName;
@property (nonatomic, strong) NSString  *trackName;
@property (nonatomic, strong) NSNumber  *albumPrice;
@property (nonatomic, strong) NSNumber  *songPrice;
@property (nonatomic, strong) NSDate    *releaseDate;
@property (nonatomic, strong) NSString  *currency;

//INFO: Add more properties as needed. I'm using the ones that are required to be displayed for now.

-(instancetype)initWithDictionary:(NSDictionary *)dictionary; // Convenient init for translating the raw dictionary.

@end