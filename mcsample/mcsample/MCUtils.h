//
//  MCUtils.h
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MCUtils : NSObject

@property (nonatomic, strong) NSDateFormatter *sharedFormatter; // Formatters are memory consumers, so we use just a shared one.

+(void)applyAppearance; //INFO: I would usually create a "Stylist" class for everything related to look and feel, but for now this does the trick.

+(instancetype)sharedInstance; // Singletons are sometimes needed, we just need to be careful with threads.

+(NSString *)yearFromDate:(NSDate *)date;


@end
