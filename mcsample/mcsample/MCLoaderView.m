//
//  MCLoaderView.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCLoaderView.h"

@implementation MCLoaderView

static MCLoaderView *instance = nil;

+(void)show {

    static dispatch_once_t onceToken; // One loader only.
    dispatch_once(&onceToken, ^{
        instance = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil][0];
    });

    instance.alpha = 0;
    instance.transform = CGAffineTransformMakeScale(0.01, 0.01);

    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:instance];
        instance.center = CGPointMake(window.frame.size.width/2, window.frame.size.height/2);

        //TODO: Add constraints in case of a screen rotation.

        [UIView animateWithDuration:0.25 animations:^{
            instance.alpha = 1;
            instance.transform = CGAffineTransformMakeScale(1, 1);
        }];
    });

}

+(void)hide {

    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25 animations:^{
            instance.alpha = 0;
            instance.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished) {
            [instance removeFromSuperview];
        }];
    });

}

@end
