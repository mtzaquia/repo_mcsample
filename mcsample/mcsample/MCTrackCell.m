//
//  MCTrackCell.m
//  mcsample
//
//  Created by Mauricio T Zaquia on 10/23/15.
//  Copyright © 2015 Venice. All rights reserved.
//

#import "MCTrackCell.h"
#import "MCAsyncLoadImageView.h"

@implementation MCTrackCell {
    CAGradientLayer *_gradient;
}


-(void)prepareForReuse {
    [super prepareForReuse];
    [self.imageAlbumCover stopLoading]; // Avoid unecessary data traffic by canceling the load when the cell is no longer displayed.
}

@end
