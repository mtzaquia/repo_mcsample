##MC Sample

The project went fine. I’m going through some topics that require observation.

- I’m applying appearance with a generic MCUtils class. I would usually create a MCStylist for appearance on larger projects;
- Since I couldn’t use third-party frameworks, I’ve implemented NSString+Encoding, which provides a query parameter friendly string and an UIImageView capable of doing async load from an URL. I would usually use CocoaPods for larger projects, as it is pretty stable now;
- I’m using #define for most of the constants. I would usually use an enumeration for that purpose in larger projects;
- Singletons were implemented with thread-safety using dispatch_once;
- Some layer properties were configured as key-value directly on storyboard instead of code;
- I’m using a DataManager for WS fetch. I consider this a nice approach;
- I’ve created a model containing only the fields required by the task, but this can be expanded;
- I would usually cache remote data using CoreData or Realm;
- I’m also using classes I’ve implemented for Loading HUD and Search Input, using xibs for those cases;
- Localization was only necessary for the UIAlertController, as other labels were either remote or on the storyboard;
- I could have use some unit tests, but the only one that would actually be vital was the integration between WS <-> DataManager <-> Model, so I’ve skipped those for now. I would strongly recommend it for bigger projects, though.
- I’m not a huge designer, opting for simpler, cleaner interfaces instead of polluted ones. I’m very used to animations and implementation of design using storyboard, though.